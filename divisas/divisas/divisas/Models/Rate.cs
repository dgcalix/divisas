﻿namespace divisas.Models
{
    public class Rate
    {
        public int RateId { get; set; }
        public string Code { get; set; }
        public string TaxRate { get; set; }
        public string Name { get; set; }
    }

}
