﻿namespace divisas.ViewModel
{
    using System.Collections.ObjectModel;
    using Models;
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using System;

    public class MainViewModel
    {
        /*Region Properties*/
        public string Amount { get; set; }
        public ObservableCollection<Rate> Rates { get; set; }
        public Rate SourceRate { get; set; }
        public Rate TargetRate { get; set; }
        public bool IsRunnig { get; set; }
        public bool IsEnable { get; set; }
        public string Result { get; set; }


        /*Region End*/


        public MainViewModel()
        {
        }

        /*Region Commads */
        public ICommand ConvertCommand
        {
            get
            {
                return new RelayCommand(Convert);
            }

        }

        private void Convert()
        {
            throw new NotImplementedException();
        }

        /*End Region Commads  */



    }
}
        